from dataclasses import dataclass
from enum import IntEnum


class Player1(IntEnum):
    A = (1,)
    B = (2,)
    C = 3

    def is_rock(self):
        return self == Player1.A

    def is_scissor(self):
        return self == Player1.B

    def is_paper(self):
        return self == Player1.C

    def get_winner(self):
        match self:
            case Player1.A:
                return Player1.B
            case Player1.B:
                return Player1.C
            case Player1.C:
                return Player1.A

    def get_loser(self):
        match self:
            case Player1.A:
                return Player1.C
            case Player1.B:
                return Player1.A
            case Player1.C:
                return Player1.B


class Player2(IntEnum):
    X = (1,)
    Y = (2,)
    Z = 3

    def is_rock(self):
        return self == Player2.X

    def is_scissor(self):
        return self == Player2.Y

    def is_paper(self):
        return self == Player2.Z


@dataclass
class Game:
    player1: Player1
    player2: Player2

    def get_result(self) -> int:
        if (
            (self.player1.is_rock() and self.player2.is_scissor())
            or (self.player1.is_scissor() and self.player2.is_paper())
            or (self.player1.is_paper() and self.player2.is_rock())
        ):
            return 6
        elif (
            (self.player2.is_rock() and self.player1.is_scissor())
            or (self.player2.is_scissor() and self.player1.is_paper())
            or (self.player2.is_paper() and self.player1.is_rock())
        ):
            return 0
        else:
            return 3

    def get_card(self) -> Player1:
        match self.player2:
            case Player2.X:
                return self.player1.get_loser()
            case Player2.Y:
                return self.player1
            case Player2.Z:
                return self.player1.get_winner()

    def score_by_strategy(self) -> int:
        match self.player2:
            case Player2.X:
                return 0 + self.get_card()
            case Player2.Y:
                return 3 + self.get_card()
            case Player2.Z:
                return 6 + self.get_card()
        return 0

    def score(self) -> int:
        return self.get_result() + int(self.player2)


class GameCalc:
    def __init__(self, games: list[Game]) -> None:
        self.games = games

    def score(self):
        sum = 0
        for g in self.games:
            sum = sum + g.score()
        return sum

    def score_by_strategy(self):
        sum=0
        for g in self.games:
            sum = sum+g.score_by_strategy()
        return sum 


@staticmethod
def new_game_calc(file_path: str) -> GameCalc:
    return GameCalc(get_data(file_path))


def get_data(file_path: str) -> list[Game]:
    results = []
    with open(file_path) as reader:
        for line in reader.readlines():
            games = line.split()
            results.append(Game(Player1[games[0]], Player2[games[1]]))
    return results


def main():
    gcalc = new_game_calc("./input.txt")
    print(gcalc.score())
    print(gcalc.score_by_strategy())


if __name__ == "__main__":
    main()
