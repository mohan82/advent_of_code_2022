from main import GameCalc
from main import Game
from main import get_data
from main import Player1
from main import Player2


def test_get_data():
    games = get_data("./test.txt")
    assert len(games) == 3


def test_winner():
    assert Player1["A"].get_winner() == Player1.B
    assert Player1["B"].get_winner() == Player1.C
    assert Player1["C"].get_winner() == Player1.A


def test_loser():
    assert Player1["A"].get_loser() == Player1.C
    assert Player1["B"].get_loser() == Player1.A
    assert Player1["C"].get_loser() == Player1.B


def test_get_data_records():
    games = get_data("./test.txt")
    result = [
        (Player1["A"], Player2["Y"]),
        (Player1["B"], Player2["X"]),
        (Player1["C"], Player2["Z"]),
    ]
    for i in range(len(result)):
        g = games[i]
        p1, p2 = result[i]
        assert g.player1 == p1
        assert g.player2 == p2


def test_winners_game():
    winners = [
        (Player1["A"], Player2["Y"], 8),
        (Player1["B"], Player2["Z"], 9),
        (Player1["C"], Player2["X"], 7),
    ]
    for w in winners:
        p1, p2, score = w
        g = Game(p1, p2)
        assert g.score() == score


def test_losers_game():
    losers = [
        (Player1["B"], Player2["X"], 1),
        (Player1["C"], Player2["Y"], 2),
        (Player1["A"], Player2["Z"], 3),
    ]
    for w in losers:
        p1, p2, score = w
        g = Game(p1, p2)
        assert g.score() == score


def test_score_by_strategy():
    gcalc = GameCalc(get_data("./test.txt"))
    assert gcalc.score_by_strategy() == 12

def test_score():
    gcalc = GameCalc(get_data("./test.txt"))
    assert gcalc.score() == 15
