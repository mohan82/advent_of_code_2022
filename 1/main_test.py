from pathlib import Path
from main import new_elves_calc
from main import ElvesCalc



def get_test_elves()->ElvesCalc:
    return new_elves_calc(Path("./test.txt"))


def test_get_data():
    elves_calc = get_test_elves()
    assert len(elves_calc.elves) == 5
    

def test_get_data_records():
    elves_calc = get_test_elves()
    results = [24000,11000,10000,6000,4000]
    for i in range(len(results)):
        assert elves_calc.elves[i].total() == results[i]


def test_max_person():
    max_calories = get_test_elves().find_max()
    assert max_calories == 24000


def test_find_top():
    elves_calc = get_test_elves()
    max_calories = elves_calc.find_top(2)
    assert len(max_calories) == 2
    assert  elves_calc.total_calories(max_calories)== 35000
