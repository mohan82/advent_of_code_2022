from pathlib import Path
from dataclasses import dataclass

INPUT_FILE = "input.txt"


@dataclass
class Elve:
    calories: list[int]

    def total(self) -> int:
        sum = 0
        for c in self.calories:
            sum = sum + c
        return sum


class ElvesCalc:
    def __init__(self, elves: list[Elve]) -> None:
        self.elves = elves

    def find_max(self) -> int:
        if self.elves:
            return self.elves[0].total()
        else:
            return 0

    def find_top(self, items: int) -> list[Elve]:
        if items > len(self.elves):
            return []
        return self.elves[:items]


    @staticmethod
    def total_calories(elves: list[Elve]) -> int:
        total = 0
        for e in elves:
            total = total + e.total()
        return total

    @staticmethod
    def _sort_by_max_calories(elves: list[Elve]) -> list[Elve]:
        return sorted(elves, key=lambda e: e.total(), reverse=True)


def new_elves_calc(file_path: Path) -> ElvesCalc:
    elves = ElvesCalc._sort_by_max_calories(get_data(file_path))
    return ElvesCalc(elves)


def get_data(file_path: Path) -> list[Elve]:
    elves = []
    with open(file_path) as reader:
        calories = []
        for line in reader.readlines():
            line = "".join(line.split())
            if line.strip(""):
                calories.append(int(line))
            else:
                elves.append(Elve(calories))
                calories = []

        if calories:
            elves.append(Elve(calories))

    return elves


def main():
    elv_calc = new_elves_calc(Path(f"./{INPUT_FILE}"))
    print(elv_calc.total_calories(elv_calc.elves))
    print(elv_calc.find_max())
    print(elv_calc.total_calories(elv_calc.find_top(3)))


if __name__ == "__main__":
    main()
